#!/bin/zsh
exepath="bin/"
cd $1/src
pwd
echo "($1) aggiorno timestamps..."
while IFS= read -r -d '' -u 9
do
    echo $REPLY $(md5 -q $REPLY) $(date +%s) >> ../../$exepath$1_time.map.t
done 9< <( find . -type f -exec printf '%s\0' {} + )

cd ../..

awk 'NR==FNR{c[$2]++;next};c[$2] == 0' $exepath$1"_time.map" $exepath$1"_time.map.t" > $exepath$1_diff.t

cat $exepath$1_time.map | while read line 
do
    # echo $line
    f=$(awk '{ print $1 }' <(echo $line))
    # echo $f
    fl=$(grep -F $f $exepath$1_diff.t)
    # echo $fl
    if [ "$fl" = "" ];
    then
    else
        echo "(✓) ho aggiornato "$f " :	" $(date +%d-%m-%Y" ore "%H:%M:%S)
        sed -i "" 's#^'$line'#'$fl'#' $exepath$1_time.map
    fi
done
diff --new-line-format="" --unchanged-line-format=""  $exepath$1_diff.t $exepath$1_time.map >> $exepath$1_time.map

rm $exepath*.t

#   run through files again to replace $var occurrencies
cd $exepath
while IFS= read -r -d '' -u 9
do
	echo $REPLY
	cp $REPLY $REPLY.bk
    cat commands/default.conf | while read line
    do
		word=$(awk '{ print $1 }' <(echo $line))
		# echo $word
		command=$(awk '{$1=""; print $0}' <(echo $line))
		if [[ $word == *"*"* ]]; then
			# echo "It's a function!"
			lang=$1
			while IFS= read -r occ;do
				if [[ $occ == *"$"*"{"*"}"* ]]; then
					cd commands
					arg=$(awk -F "[{}]" '{ for (i=2; i<NF; i+=2) print $i }' <(echo $occ))
					command=$(awk '{ print $1 }' <(echo $command))
					output=$(eval $command $lang $arg)
					tmp=$(awk -F "[\$}]" '{ for (i=2; i<NF; i+=2) print $i }' <(echo $occ))
					toreplace=\$$tmp'}'
					sed -i '' 's#\'$toreplace'#'$output'#' ../$REPLY
					cd ..
				fi
			done < <(grep -i "$word" $REPLY)
		else
			out=$(eval $command)
			# echo $out
			sed -i '' 's/\'$word'/'$out'/' $REPLY
		fi
    done
done 9< <( find ../$1/src -type f -name '*.md' -exec printf '%s\0' {} + )