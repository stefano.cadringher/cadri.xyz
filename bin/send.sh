#!/bin/zsh
PATH="$HOME/Documents/htdocs/Website/bin:$PATH"
cd ..

./bin/sync.sh en
./bin/sync.sh it

./bin/ssg5 it/src it/web 'Blog di Stefano' 'https://it.cadringher.dev' &> /dev/null
./bin/ssg5 en/src en/web 'Stefanos blog' 'https://en.cadringher.dev' &> /dev/null
cp -R common/* it/web/
cp -R common/* en/web/

if [ "$1" != "--quick" ]; then
    #   compilo l'ultima versione del CV e la copio
    #/./Users/stefano/Documents/CV/compile.sh
    #cp /Users/stefano/Documents/CV/IT/StefanoCadringherIT.pdf common/StefanoCadringherIT.pdf
    #cp /Users/stefano/Documents/CV/EN/StefanoCadringherEN.pdf common/StefanoCadringherEN.pdf
    echo "(-) Compilazione e copia del curriculum sospesi.    "

    echo -e "\e[1mModifiche apportate:\e[0m"
    echo "${$({ git diff --name-only ; git diff --name-only --staged ; } | sort | uniq)/ /\n}"

    echo "\n.. Aggiorno la repository... "
    git add . &> /dev/null
    git commit -m "update $(date)" &> /dev/null
    git push -u origin master &> /dev/null
    tput cuu1
    tput cuu1
    echo "(✓) Repository aggiornata.    "
    git log --pretty=format:"%H - %an" | head -n 1
    tput cud1
fi

if [ "$1" != "--update-repository" ]; then
    surge it/web it.cadringher.dev
    surge en/web en.cadringher.dev
    surge default cadringher.dev
fi

cd en/src
rm *.md
#	lmao risky
rename "s/md.bk$/md/" *.md.mk

cd it/src
rm *.md
#	lmao risky
rename "s/md.bk$/md/" *.md.bk
