# Contattami
## Email e chiave pubblica
* Email: [stefano.cadringher@nacheso.com](mailto:stefano.cadringher@nacheso.com)
* [Public Key](https://keybase.io/nacheso): `5338 6046 5E7A 0658 2E7A  1D79 E5AF 5F49 AD2D FDEC`

## Social network, GitLab ecc
* GitLab personale: [@s.cadringher](https://gitlab.com/s.cadringher)
* GitLab Nacheso: [@stefano.cadringher](https://gitlab.com/stefano.cadringher)
* Twitter [@ste_cadringher](https://twitter.com/ste_cadringher)
* Telegram [@scadringher](https://t.me/scadringher)