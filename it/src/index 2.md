# Benvenuti nel mio blog!
## Chi sono
Sono Stefano Cadringher, un **programmatore freelancer**, fondatore di Nacheso Software e studente di **Informatica, sistemistica e comunicazione** all'[Università degli studi di Milano-Bicocca](https://www.unimib.it). Sono appassionato di free software e della sua filosofia, sistemi operativi UNIX-like e tecnologia in generale.

## Cosa voglio fare
Attraverso questo microblog cerco di **condividere con il mondo** alcune delle cose che imparo lungo il mio percorso, utilizzando questo sito come [repository](articles.html) di tutti i progetti più o meno complessi che mi capitano.

## Esperienza passata
Ho scritto e tutt'ora manutengo piccoli CRM, webapp e siti web per alcune aziende, ho sviluppato un URL shortener abbastanza popolare (nchs.me) che ho dovuto chiudere e un paio di bot Telegram (uno aveva quasi 2000 utenti), tutti facenti parte dell'ecosistema [nacheso](https://nacheso.com), un sistema di microservizi che io e un mio amico avevamo scritto. 
