# Articoli pubblicati
Ecco una lista dei miei articoli:

* [Come ho scritto questo sito](articles/how-to-build-a-website.html) *Ultimo aggiornamento il 2020-07-03 15:13*
* [Come ho chiuso il mio URL shortener](articles/the-url-shortener-story.html) *Ultimo aggiornamento il 2020-07-03 09:20*