# Come creare un semplice sito con `ssg5`
### <a class=backbtn href=/articles>← Pagina precedente</a>
*Ultimo aggiornamento 2020-07-03 15:13*
> Per la scrittura di questo articolo (e la creazione di questo sito) ho preso ispirazione da [questo video](https://www.youtube.com/watch?v=N_ttw2Dihn8) di uno dei miei content creator preferiti. Se vuoi saperne di più su `ssg5` e tutte le sue funzionalità, **ti consiglio di visitare il sito del suo creatore**, linkato nel footer di ogni pagina.

## Nozioni di base
`ssg5` combinato con [lowdown](https://kristaps.bsd.lv/lowdown/) oppure [Markdown.pl](https://daringfireball.net/projects/markdown/) essenzialmente *compila* i tuoi documenti [markdown](https://en.wikipedia.org/wiki/Markdown) in HTML, aggiungendo un'intestazione precompilata in testa alla pagina e un footer in basso. Si può inoltre aggiungere uno `style.css` nella cartella `src`.

## Installazione
### Download
<pre>
mkdir -p bin
curl -s https://rgz.ee/bin/ssg5 > bin/ssg5
curl -s https://rgz.ee/bin/Markdown.pl > bin/Markdown.pl
chmod +x bin/ssg5 bin/Markdown.pl
</pre>
Per questa installazione andremo ad utilizzare `Markdown.pl` perchè vogliamo convertire i file `.md` in file HTML.

### Setup e primo avvio
Devi ora assicurarti che sia `ssg5` che `Markdown.pl` siano nella variabile `$PATH`, e che le cartelle `src` e `dst` esistano e siano accessibili dall'utente desiderato. Puoi chiamare queste ultime due cartelle come vuoi. Per ragioni pratiche io ad esempio ho rinominato `dst` in `web`. 
<pre>
PATH="wherever/your/bin/is:$PATH"
mkdir src dst
</pre>
Ora dobbiamo riempire la cartella `src` con delle pagine in markdown. Per farlo iniziamo scrivendo qualcosa in un file chiamato `index.md`, nella cartella `src`, come nell'esempio sotto:

![alt text](/images/sublime-index-markdown.png "index.md example")

Ora bisogna aggiungere un header e un footer nella cartella `src` per includere file css, aggiungere una navbar o comunque se si desidera avere degli elementi presenti in ogni pagina. Per aggiungere header e footer bisogna creare `_header.html` e `_footer.html` nella cartella `src` e riempirli con il contenuto desiderato.

#### Il mio file `_header.html`:
<pre>
<&zwj;html><&zwj;title><&zwj;/title>
<&zwj;meta charset="UTF-8">
<&zwj;link rel="alternate" type="application/atom+xml" href="/rss.xml">
<&zwj;link rel="stylesheet" href="/style.css">
<&zwj;meta name="viewport" content="width=device-width, initial-scale=1.0">
...
</pre>

#### Il mio file `_footer.html`:
<pre>
<&zwj;div class="footer">
	<&zwj;div class="menuitem">
		<&zwj;a class="m" href="https://en.cadri.xyz">🇬🇧 English version<&zwj;/a> 
	<&zwj;/div>
	<&zwj;br>
	&copy; <&zwj;a href="contacts.html">Stefano Cadringher<&zwj;/a>, 2020 &mdash; Powered by a modified version of <&zwj;a href="https://www.romanzolotarev.com">Roman Zolotarev's</&zwj;a> <&zwj;a href="https://www.romanzolotarev.com/ssg.html">ssg5<&zwj;/a>
<&zwj;/div>
...
</pre>

Ora basta eseguire `ssg5` con questo comando
<pre>
bin/ssg5 src dst 'Test' 'http://www.yourwebsite.com'
</pre>
Dove `bin` è la cartella dove `ssg5` è stato scaricato, `src` è la cartella dove metti tutti i file `.md` e `dst` la cartella di destinazione. **Test** sarà il nome del sito, che apparirà dopo il nome della singola pagina nella barra del titolo del browser. Puoi cambiare **Test** con quello che vuoi. **L'ultimo parametro è richiesto** se vuoi i feed RSS (non in questo tutorial) e la mappa del sito in `xml`, generata automaticamente da `ssg5`. Il comando per questo sito in versione italiana è questo, ad esempio:
<pre>
bin/ssg5 src web 'Stefanos blog' 'https://en.cadri.xyz'
</pre>

![alt text](/images/dst-folder-finder.png "index.md example")

## Ultimi passi
Ok hai una pagina funzionante (spero). Bene, ora ti spiego come ho costruito **questo sito**. Mi serviva un livello di complessità leggermente maggiore dato che avrei dovuto scrivere questi articoli in due lingue e **volevo poter condividere file statici tra pagine di entrambi i sottositi**, tenendo però il numero di cartelle comprensibile e minimale sul server e sul mio computer.

### Folder structure

![alt text](/images/folder-file-list.png "index.md example")

La mia soluzione è molto semplice e non ha richiesto un particolare sforzo, ma mi trovo abbastanza bene. Ho messo tutti i file che volevo fossero condivisi tra i vari siti (sono differenti *virtual server* su nginx) in una cartella chiamata `common`. Ho poi scritto un semplice script per "compilare" sia la versione in inglese che quella in italiano con `ssg5`, così:
<pre>
#!/bin/sh
PATH="$HOME/Documents/Website/bin:$PATH"
./bin/ssg5 it/src it/web 'Blog di Stefano' 'https://it.cadri.xyz'
./bin/ssg5 en/src en/web 'Stefanos blog' 'https://en.cadri.xyz'
cp -R common/* it/web/
cp -R common/* en/web/

scp -r it/web/* webserveruser@cadri.xyz:/srv/web/it
scp -r en/web/* webserveruser@cadri.xyz:/srv/web/en
</pre>
Come puoi vedere, compilo il mio sito dalla cartella `src` di ogni lingua, e una volta compilata in locale nella cartella `web`, copio i contenuti statici all'interno di ognuna delle cartelle `web`, facendo sì che i link all'interno dei file html rimangano relativi alla cartella in cui sono contenuti. Carico le cartelle web su questo server usando `scp` con chiavi ssh condivise, per tenere tutto sicuro.
<pre>
$ tree
.
|-- en
|   |-- articles
|   |   |-- how-to-build-a-website.html
|   |   `-- the-url-shortener-story.html
|   |-- articles.html
|   |-- contacts.html
|   |-- favicon.ico
|   |-- images
|   |   |-- dst-folder-finder.png
|   |   |-- favicon.ico
|   |   |-- folder-file-list.png
|   |   |-- logo.svg
|   |   |-- sublime-index-markdown.png
|   |   `-- website-folder-finder.png
|   |-- index.html
|   |-- logo.svg
|   |-- sitemap.xml
|   `-- style.css
`-- it
    |-- articles
    |   `-- the-url-shortener-story.html
    |-- articles.html
    |-- contacts.html
    |-- favicon.ico
    |-- images
    |   |-- dst-folder-finder.png
    |   |-- favicon.ico
    |   |-- folder-file-list.png
    |   |-- logo.svg
    |   |-- sublime-index-markdown.png
    |   `-- website-folder-finder.png
    |-- index.html
    |-- logo.svg
    |-- sitemap.xml
    `-- style.css

6 directories, 29 files
</pre>
struttura delle cartelle sul server al momento della scrittura.
