# Contacts  
## Email
* Email: [s.cadringher@gmail.com](mailto:s.cadringher@gmail.com)
* School email: [sc4294@drexel.edu](mailto:sc4294@drexel.edu)
* [Public Key](https://keybase.io/nacheso): `533860465E7A06582E7A1D79E5AF5F49AD2DFDEC`

## Social and not so social networks
* Personal GitLab: [@s.cadringher](https://gitlab.com/s.cadringher)
* Personal GitHub: [@stecadri](https://github.com/stecadri)
* Work GitLab [@stefano.cadringher](https://gitlab.com/stefano.cadringher)
* Twitter [@ste_cadringher](https://twitter.com/ste_cadringher)
* Telegram [@scadringher](https://t.me/scadringher)
* Instagram [@ste_cadri](https://instagram.com/ste_cadri)

Fri May  3 12:48:52 EDT 2024