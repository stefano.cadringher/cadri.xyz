# Articles
Here's a list of my recent articles:
 
<ul>
    <li><a href="articles/hand-gesture-recognition-cnn.html">Using a custom CNN for simple hand gesture recognition</a><br><em> Last updated 04-05-2024 11:23:22</em></li>
    <li><a href="articles/seam-carving-matlab.html">Simple seam carving project written in Matlab</a><br><em> Last updated 04-03-2024 10:04:43</em></li>
    <li><a href="articles/the-url-shortener-story.html">The URL Shortener Story</a><br><em> Last updated 02-02-2022 17:17:52</em></li>
    <li><a href="articles/how-to-build-a-website.html">How to build a simple website with `ssg5`</a><br><em> Last updated 02-02-2022 21:32:40</em></li>
</ul>  