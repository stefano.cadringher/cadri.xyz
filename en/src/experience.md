# My experience

<div class="topbar">
<div class="menuitem">
    <a class="m" href="https://www.linkedin.com/in/cadringher">👤 LinkedIn</a>
</div>
<div class="menuitem">
    <a class="m" target="_blank" href="https://bit.ly/sc_resumeA004"">📄 Resume</a>
</div>
</div>

## Education
- 🇺🇸 2023 - 2025 (est.): [**_Drexel University_**](https://drexel.edu), Philadelphia, PA: MS (Master's Degree in AI&ML). **Relevant Coursework:** Machine Learning, Deep Learning, Computer Vision, Natural Language Processing, and Human-AI interaction

- 🇮🇹 2019 - 2023: [**_Università degli Studi di Milano-Bicocca_**](https://en.unimib.it), Milan, Italy: BCS (Bachelors Degree in Computer Science). **Relevant Coursework:** Programming, Networks and Operating Systems, Programming Languages and Databases

- 🇸🇪 2021 - 2022: [_**Högskolan i Skövde**_](https://his.se), Skövde, Sweden: Exchange Student. **Relevant Coursework:** Distributed Systems, Dynamic Programming, Virtualization, Cloud and Storage Systems.

## Work Experience
- 🇺🇸 Sep. 2022 - Jul. 2023: [_**Drexel University**_](https://swre.cae.drexel.edu/members/), Philadelphia, PA, USA: <br> **Research** at the CAEE (Civil, Architechtural and Environmental Engineering) Lab. NSF Funded grant to create an [app](https://apps.apple.com/us/app/cleanlet/id1667911208) that would reduce the risk of flooding in flood prone areas. <br>
_Technologies used:_ Flutter, Firebase, Firestore, Dart, InfluxDB

### Work as Nacheso Sotware

- 🇸🇪/🇮🇹 Jan. 2022 - Jun. 2022: _**JLR Radio**_, Skövde, Sweden/Milan, Italy: <br>
• Built a web radio software used in 100+ Jaguar Land Rover dealerships in Italy. 
• Reduced ad playback logging time by 80% using a distributed approach.
• Increased uptime by over 40%.

- 🇮🇹 Mar. 2021 - Jan. 2022: [_**ITCube Consulting**_](https://itcube.it), Milan, Italy: <br>Worked as a consultant from march to december, I was expected to estimate ETAs, prices, speak directly to the customers and assemble the right workgroup for the task at hand. 

- 🇮🇹 Mar. 2020 - Jan. 2021: [_**Consorzio Nazionale Batterie**_](https://sinab.eu), Paderno Dugnano, Italy: <br>
Streamlined the process of Wordpress website creation though cloud solution orchestration. Developed websites for more than 10 clients in the environmental sector.

- 🇮🇹 Jan. 2020 - Jun. 2020: _**i-TECH Srl**_, Sassari, Italy: <br>I created a web portal that receives, displays and handles
new customers, with the ability to sort, search, and integrate their workflow seamlessly into the
program. It shows meaningful data regarding the customer’s origins and gives the company a
tool to better direct their marketing efforts as well as their web presence among italian regions

- 🇮🇹 Oct. 2019 - present: [_**M.P.M. Ambiente S.r.l.**_](https://www.mpmambiente.it/sito/), Busnago, Italy: <br>My task is to maintain existing services, add new features on request, develop software and update their public website as well as their social presence as needed.

- 🇮🇹 Oct. 2018 - Oct. 2019: [_**M.P.M. Ambiente S.r.l.**_](https://www.mpmambiente.it/sito/), Busnago, Italy: <br>I developed mobile friendly platform used by the employees to track, elaborate and store data generated from their site inspections. This data is automatically organized and ready to be sent to the parent company, and complies with the current european regulations. Besides handling inspections data the program also gives supervisors useful insights on the performance of the employees and keeps track of the current stock inventory.

## AI-related Personal Projects
- 🇺🇸 May 2024 - June 2024: [**Text summarization using NLP**](), Philadelphia, PA:<br>(coming soon)

- 🇺🇸 Apr. 2024: [**Philly Codefest 2024**](https://event.phillycodefest.com/), Philadelphia, PA:<br> Developed _Civitas_, a concept app to crowdsource the maintainance a city's public infrastructure using Computer Vision to detect possible faults and Machine Learning to train an object recognition model. Everything was developed using AWS (Lambda, S3, DynamoDB and Sagemaker)

- 🇺🇸 Apr. 2024: [**Hand gesture recognition**](/articles/hand-gesture-recognition-cnn.html), Philadelphia, PA:<br> Developed a CNN from scratch to recognise live hand movement and positioning from a camera feed. This project was developed in Python without using common ML libraries. Read more on my blog!

- 🇺🇸 Dec. 2023: [**PA Traffic prediction using KNN**](), Philadelphia, PA:<br> 
Research project on an ensamble ML approach to traffic and accident severity prediction in the PA/Philadelphia area. 

- 🇺🇸 Oct. 2023: [**Seam Carving using MATLAB**](/articles/seam-carving-matlab.html), Philadelphia, PA:<br> Developed a MATLAB project to apply seam carving to images. For this project, no common image manipulation libraries were used. Read more on my blog!

- 🇺🇸 Feb. 2023 - May 2023: [**Water level detection using TensorFlow**](), Philadelphia, PA:<br> Part of my research job was to find innovative ways to detect flooding level on public streets. I experimented using a [SenseCAP A1101](https://www.seeedstudio.com/SenseCAP-A1101-LoRaWAN-Vision-AI-Sensor-p-5367.html) and TensorFlow to assess water level using camera feed and on-device computing.

- 🇮🇹 Oct. 2017 - Nov. 2017: [**Simple MNIST recognition using C++**](), Carate Brianza, MB<br> High-school voluntary project to explore AI, I chose to develop a handwriting recognition program in C++ that used the MNIST dataset. 

## Internships and school-sponsored Work

- 🇮🇹 Jun. 2018 - Jul. 2018: [_**A.Manzoni Advertising, S.p.A.**_](https://www.gedi.it/en), Milano, Italy: <br>After my work experience there, I was asked by them to create an internal website containing all the resources for new work experience students. I was also asked to develop a small program used by the helpdesk to recieve tickets and not having to rely on excel sheets.

- 🇬🇧 Jun. 2018 - Jul. 2018: [_**WSP London**_](https://wsp.com), London, UK: <br>Summer internship at WSP London, world leading engineering firm.

- 🇮🇹 Apr. 2018 - May. 2018: _**Istituto Scolastico Don Carlo Gnocchi**_, <br>I was tasked with organizing, redacting, and giving a total of 5 lessons on the topic of web programming, in collaboration with my at the time professor Diego Mansi.

- 🇮🇹 Jun. 2017 - Jul. 2017: [_**A.Manzoni Advertising, S.p.A.**_](https://www.gedi.it/en), Milano, Italy:  <br>Company’s sector: Advertising i Work experience in the IT department, where I recieved training on the basics of Microsoft Access and the concept of database. I was also given a tour of the company’s Windows Active Directory and SAP.

## Campus involvment

- Vice President, **Italian Pride Drexel**
- Full Member, **Drexel Society of Artificial Intelligence**

## Mentorship Programs

- 🌐 [_**Lead The Future (LTF)**_](https://www.leadthefuture.tech/): Actively engaged in LTF, an elite STEM mentorship program with less than a 12% acceptance rate.

## Conferences

- 🇺🇸 **ASCE EWRI** _(Henderson, NV, USA)_ <br> **Presentation title**: Community-led Stormwater Infrastructure Maintenance Using Real Time and Technology-Enabled Solutions for Climate Resilience

## Skills
<div class="topbar">
    <div class="menuitem">
        <a class="m skill" href="">C++</a>
    </div>
        <div class="menuitem">
        <a class="m skill" href="">Matlab</a>
    </div>
        <div class="menuitem">
        <a class="m skill" href="">Python</a>
    </div>
        <div class="menuitem">
        <a class="m skill" href="">JS</a>
    </div>
        <div class="menuitem">
        <a class="m skill" href="">Dart</a>
    </div>
</div>
<div class="topbar">
        <div class="menuitem">
        <a class="m skill" href="">&nbsp;ML&nbsp;</a>
    </div>
            <div class="menuitem">
        <a class="m skill" href="">Deep Learning</a>
    </div>
            <div class="menuitem">
        <a class="m skill" href="">CV</a>
    </div>
    <div class="menuitem">
        <a class="m skill" href="https://www.linkedin.com/in/cadringher">•••</a>
    </div>
</div>

Tue May  7 12:15:35 EDT 2024