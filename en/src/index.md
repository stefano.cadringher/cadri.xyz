# Welcome to Ste's blog! 🇮🇹 🇪🇺

> <h3>Latest Article:</h3><a href="articles/hand-gesture-recognition-cnn.html">Using a custom CNN for simple hand gesture recognition</a><br><em> Last updated 04-05-2024 11:23:22</em>

## Who am I?

I am Stefano Cadringher, founder of Nacheso Software. I am based in Philly and I'm currently an **Artificial Intelligence and Machine Learning student** at [Drexel University](https://drexel.edu). I'm very passionate about Machine Learning, Deep Learning, General Artificial Intelligence and Human Interaction.

## What I'm looking for
I am looking for exciting new opportunities here in the US, where I moved to pursue a **career in the AI/ML space**. I am looking for like-minded people and mentors that can help me achieve my dream of being a leader in this sector. In the long run, I'd like to work on my own startup, and create something useful and cool.

Currently looking for internships for Summer 2024! 💻

## Past experience
Since I moved across the ocean, and before eventually pursuing my Master's, I covered a Software Research role at the College of Engineering, in the CAEE department at Drexel University. I worked in the [SWRE lab](https://research.coe.drexel.edu/caee/swre/members/) to help develop an [NSF-funded](https://www.nsf.gov/awardsearch/showAward?AWD_ID=2141192&HistoricalAwards=false) mobile [app](https://apps.apple.com/us/app/cleanlet/id1667911208) to mitigate flooding in flood prone areas.

Back in the old continent, I built and still maintain small CRMs, webapps and websites for multiple companies, coded a fairly popular URL shortener (nchs.me), and a few telegram bots [(one of them was kinda popular, reaching 2000 users in less than a year)](http://www.telegramitalia.it/nacheso-youtube-downloader/). Find more about me in the [Experience](experience.html) section!

I started coding in 2015, so there's lots to unpack! Feel free to [contact me!](contacts.html)

Fri May  3 12:48:52 EDT 2024