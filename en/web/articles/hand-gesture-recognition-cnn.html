<html id="web"><title>Using a custom CNN for simple hand gesture recognition &mdash; Stefanos blog</title>
<meta charset="UTF-8">
<link rel="alternate" type="application/atom+xml" href="/rss.xml">
<link rel="stylesheet" href="/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
<script src="/n.js"></script>
<link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@400&family=Questrial&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400&display=swap" rel="stylesheet">
<!--<script type = "text/javascript">  
window.onload = function() {
    var pre = document.getElementsByTagName('pre'),
        pl = pre.length;
    for (var i = 0; i < pl; i++) {
        pre[i].innerHTML = '<span class="line-number"></span>' + pre[i].innerHTML + '<span class="cl"></span>';
        var num = pre[i].innerHTML.split(/\n/).length;
        for (var j = 0; j < num; j++) {
            var line_num = pre[i].getElementsByTagName('span')[0];
            line_num.innerHTML += '<span>' + (j + 1) + '</span>';
        }
    }
};-->
</script>
<div class="topbar">
	<div class="logo-container">
		<img src="https://gitlab.com/stefano.cadringher/cadri.xyz/-/raw/master/common/images/logo.svg" height="100%">
	</div>
	<div class="menuitem">
		<a class="m" href="/index.html">Home</a>
	</div>
	<div class="menuitem">
		<a class="m" href="/contacts.html">@me</a>
	</div>
	<div class="menuitem">
		<a class="m" href="/articles.html">Blog</a>
	</div>
	<div class="menuitem">
		<a class="m" href="/experience.html">Work</a>
	</div>
</div>
<h1>Using a custom CNN for simple hand gesture recognition</h1>

<div class="topbar">
<div class="menuitem">
    <a class="m" target="_blank" href=""">🖥️ GitHub</a>
</div>

<div class="menuitem">
    <a class="m" target="_blank" href="https://www.youtube.com/playlist?list=PLs1gMn10ZiybMB-35JsIq4YnGaq6I5KKi">📄 Paper</a>
</div>

<p></div></p>

<h3><a class=backbtn href=/articles>← Previous page</a></h3>

<p><em>Last updated Sat  4 May 2024 11:23:22 EST</em></p>

<blockquote>
  <p>In this article we're going to explore a simple yet powerful way to build and train a dataset for hand gesture recognition, specifically still hands. We are also going to explore applications of this approach on moving hands, and possible future developements to make this a complete real-time ASL interpreter.</p>
</blockquote>

<h1>Background and related work (main works cited)</h1>

<ul>
<li><a href="https://doi.org/https://doi.org/10.1016/j.iswa.2021.200056">Sign Language Recognition (Science Direct)</a></li>
<li><a href="https://www.kaggle.com/code/bhaveshmittal/asl-recognition-using-cnn-99-97-accuracy/notebook">ASL Recognition (Kaggle)</a></li>
<li><a href="(https://digitalcommons.kennesaw.edu/cgi/viewcontent.cgi?article=1024&amp;context=cs_etd">American Sign Language (Kennesaw State University)</a>)</li>
</ul>

<h1>Problem definition</h1>

<h2>Still movement</h2>

<p>It is pertinent to address a specific limitation of this CNN model in recognizing the letters ’J’ and ’Z’ within American Sign Language (ASL) spelling. This constraint stems from the <strong>model’s reliance on still images to identify letters.</strong> In ASL, both ’J’ and ’Z’ are represented not only through specific hand shapes but also through distinctive movements—’J’ is conveyed by drawing a ’J’ shape in the air, and ’Z’ involves tracing a ’Z’ pattern.</p>

<h2>Possible solutions</h2>

<p>The main idea for capturing and identifying movement saving a 3-frame rolling picture, and using that to recognise the letters. </p>

<p><img src="/images/dataset-composite.jpg"></p>

<h1>Dataset</h1>

<h2>Original idea</h2>

<p>The original dataset found on Keggle contained 27,455 training images, 7,172 testing images. All the images are 28x28 in resolution and 8-bit RGB.</p>

<p><img src="/images/mnist_original.png"></p>

<h2>Problem</h2>

<p>In the development of our convolutional neural network (CNN) for real-world American Sign Language (ASL) recognition, significant limitations with the initial dataset were encountered, sourced from MNIST. While MNIST has been instrumental for benchmarking machine learning models, we found that <strong>its "too perfect" nature—comprising images that lack the diversity and complexity of real-world scenarios—did not translate well for the intended purposes.</strong> Specifically, the dataset presented diversity issues and was too limited in representing the nuanced variations of ASL signs, including differences in hand shapes, sizes, and orientations, as well as the presence of dynamic movements for certain letters. Recognizing these shortcomings, the best idea was to create a proprietary dataset, leveraging <strong>OpenCV and MediaPipe for image capture and processing.</strong> This approach made it possible to generate a more comprehensive and diverse collection of hand gesture images, encompassing a wider array of real-world conditions. By customizing our dataset, the aim was to <strong>enhance the robustness and applicability of our CNN</strong>, ensuring it is better equipped to accurately recognize ASL spelling in diverse and practical environments.</p>

<h2>Solution</h2>

<p>300 pictures for each hand (left/right) and for each letter, about 12,000 images total. Each image is 128x128 in size, and black/white. Each letter is contained in a folder, and labeled with the appropriate hand (left or right) and the epoch at which it was taken.</p>

<p><img src="/images/folder_hands.png"></p>

<h2>Ingestion pipeline</h2>

<p>Utilizing the OpenCV library and MediaPipe for hand detection, the pipeline continuously monitors video input from a webcam, identifying hand positions and movements. <strong>This pipeline processes each frame to detect hand gestures, and when a new gesture is detected, it captures a grayscale image of the hand.</strong> To maintain data relevance and reduce redundancy, it compares consecutive images using histogram comparison for significant differences, discarding those too similar and thus capturing only distinct gestures. </p>

<p><img src="/images/pipeline_hands.png"></p>

<h1>Design decisions</h1>

<h2>LeNet Architecture</h2>

<p>The <a href="http://vision.stanford.edu/cs598_spring07/papers/Lecun98.pdf">LeNet Architecture</a> was chosen due to it being a common, simplistic, and one of the first architecture to be used in this space. It was created by Yann LeCun back in 1998 while working at Bell Labs. It has gone through multiple iterations since then. This consists of four main layers which are the <strong>Convolution, ReLU, Pooling, and Classification Layer.</strong></p>

<p><img style="box-shadow:none" src="/images/LeNet.png"></p>

<h2>Hyperparameters</h2>

<p>The hyperparameters were initialized as below:</p>

<h3>Kernel</h3>

<p>Initialized the kernel as a size of 9 x 9 and a stride of 9 due to the fact this will reduce the sub-image down to a 3 x 3 matrix. The weights were also initialized using Xavier initialization.</p>

<h3>Weights</h3>

<p>Initialize the weights using a Gaussian Distribution due to it being the best fit following a fully connected layer.</p>

<h3>Learning Rate and Stoppage</h3>

<p>Utilized a Learning rate of 0.00001 and stoppage of 10E-7 to keep the model from stopping too soon</p>

<h3>ADAM</h3>

<p>Multiple saddle points where observed in the learning of this model and ADAM helped mitigate the model from being stuck</p>

<h1>Evaluation</h1>

<h2>Convolution Layer Standard approach</h2>

<p>Iterated over each image and performed the <strong>sliding DOT product</strong> by taking a filter and sliding over portions of the image. This required looping through the Height and Width of the Kernel while being bound by the images Height and Width. <strong>This can be a cumbersome due to our O(n) time be reliant on Image Count (IMC), Kernel height (KNH), and Kernel width (KNW)</strong> leaving us with a time:   </p>

<p><img id=eq src="https://latex.codecogs.com/svg.image?IMC^{KNH x KNW}"></p>

<h2>Optimized approach</h2>

<p>Utilized a Toeplitz matrix which is a matrix where it's diagonals and sub-diagonals are constant. These Toeplitz matrices are then stacked on top of and adjacent to one another based off the the resulting sub-image dimensions. <strong>This then allows for a the creation of sequence when flattened out.</strong> This will match the orientation of the flattened image mimicking the sliding DOT product, but as a matrix computation instead of a iterative loop in order to generate a flattened Kernel. Our O(n) would be reliant on Image size (IMS), Kernel height (KNH), and Kernel width (KNW) leaving us with a time of </p>

<p><img id=eq src="https://latex.codecogs.com/svg.image?IMS**(KNH^{KNW})"></p>

<h2>I &amp; W</h2>

<p><img id=eq src="https://latex.codecogs.com/svg.image?W = \begin{bmatrix} w_{0, 0} &amp; w_{0, 1} &amp; w_{0, 2} \\ w_{1, 0} &amp; w_{1, 1} &amp; w_{1, 2} \\ w_{2, 0} &amp; w_{2, 1} &amp; w_{2, 2} \end{bmatrix}"></p>

<p><img id=eq src="https://latex.codecogs.com/svg.image?I = 
\begin{bmatrix}
i_{0, 0} &amp; i_{0, 1} &amp; i_{0, 2} &amp; i_{0, 3} \\
i_{1, 0} &amp; i_{1, 1} &amp; i_{1, 2} &amp; i_{1, 3} \\
i_{2, 0} &amp; i_{2, 1} &amp; i_{2, 2} &amp; i_{2, 3} \\
i_{3, 0} &amp; i_{3, 1} &amp; i_{3, 2} &amp; i_{3, 3} \\ 
\end{bmatrix} 
\in
\mathbb{R}^{4 \times 4}"></p>

<h1>Benchmark</h1>

<p>8 times faster!!</p>

<table class="tg">
<thead>
  <tr>
    <th><br></th>
    <th>Non optimized&nbsp;&nbsp;</th>
    <th><b>Optimized</b></th>
    <th>∆</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>100 epochs</td>
    <td>23 sec</td>
    <td>2 min</td>
    <td>97 sec</td>
  </tr>
  <tr>
    <td>10k epochs</td>
    <td>39 min</td>
    <td>5 hrs</td>
    <td>4.3 hrs</td>
  </tr>
</tbody>
</table>

<h1>Future work</h1>

<h2>Real time recognition</h2>

<p>In future developments, the goal is to evolve this system into one <strong>capable of real-time hand gesture recognition</strong> by integrating the trained CNN directly with the live feed capture process.  </p>

<p>This means <strong>reversing the current ingestion pipeline's flow</strong>: instead of saving composite images for later processing, the script will dynamically create these composites in real-time and immediately feed them into the CNN for gesture classification. <strong>As the camera captures video, the script will continuously extract sequences of the frames that exhibit potential hand movements</strong>, preprocess these sequences into the standardized composite format, and then input them into the CNN model.</p>

<h2>Hand movement recognition</h2>

<p>It would also be possible to recognize movement using t<strong>he three-picture approach</strong> we discussed earlier. This would drastically increase the training time, as the inputs would change in size from 128x128 to 384x128. A major challenge with this approach would be the timing of the movement: a slower or faster hand movement would probably change the accuracy of the predicted result.</p>

<h2>More optimization</h2>

<p>Currently it's still a challenge to blow up the image leaving the Convoluted layer in order to run our Max Pooling layer. <strong>Going forward it's going to be necessary to use Maxing pooling while the images are still flattened.</strong> Doing so we would remove an extra flattening layer step from both forward and backwards and tremendously reduce over compute time for the Max Pooling layer. </p>

<h1>Results</h1>

<p>Training data is shown in blue and validation data is shown in orange.</p>

<p><img src="/images/result-cnn.png"></p>

<p>An accuracy of 90% was obtained after 10,000 epochs.</p>

<h1>Conclusions</h1>

<p>Throughout this project - a perhaps too ambitious journey to implement live hand gesture recognition using CNNs - challenges were faced in achieving real-time hand recognition and comprehensive hand movement recognition, these efforts have not been without significant breakthroughs. <strong>The most notable accomplishment of this work has been the substantial improvement in the efficiency</strong> of our CNN model, making it <strong>8 times faster</strong> than the starting point.</p>
<div class="footer">
	<div class="menuitem"> 
		<a class="m" href="https://it.cadringher.dev">🇮🇹 in Italiano</a> 
		<a class="m nm" style="cursor: pointer;"onclick="toggleNightMode()">✨ Night mode</a> 
	</div>
	<br><br>
	&copy; <a href="contacts.html">Stefano Cadringher</a> @ Nacheso Software, 2022 &mdash; VAT: (IT)11328220964<br>
	Powered by a modified version of <a href="https://www.romanzolotarev.com">Roman Zolotarev's</a> <a href="https://www.romanzolotarev.com/ssg.html">ssg5</a>
</div>
