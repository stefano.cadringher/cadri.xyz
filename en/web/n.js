function nightModeOn () {
    $('a').css({
        'background-color'  : 'rgb(198,198,198,0.25)',
        'color'             : 'white'
    }); $('html').css({
        'background-color'  : 'black',
        'color'             : 'white'
    }); $('pre').css({
        'color'             : 'white',
        'background-color'  : '#ffffff20',
    }); $('blockquote').css({
        'color'             : 'white',
        'background-color'  : '#ffffff30',
        'border-left'       : '.5em solid #ffffff16'
    }); $('.nm').css({
        'color'             : 'black',
        'background-color'  : 'white'
    });
}

window.onload = function() {
    var pre = document.getElementsByTagName('pre'),
        pl = pre.length;
    for (var i = 0; i < pl; i++) {
        pre[i].innerHTML = '<span class="line-number"></span>' + pre[i].innerHTML + '<span class="cl"></span>';
        var num = pre[i].innerHTML.split(/\n/).length;
        for (var j = 1; j < num; j++) {
            var line_num = pre[i].getElementsByTagName('span')[0];
            line_num.innerHTML += '<span>' + (j) + '</span>';
        }
    }
};

$(function(){ 
    if (Cookies.get('nightmode') == 'true')  {
        nightModeOn();
    }
});

function toggleNightMode () {
    if (Cookies.get('nightmode') == 'true') {
        Cookies.set('nightmode', 'false', { secure:true });
        location.reload();
    } else {
        Cookies.set('nightmode', 'true', { secure:true });
        nightModeOn();
    }
}

if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    if (Cookies.get('nightmode') == 'false') {
        Cookies.set('nightmode', 'true', { secure:true });
        location.reload();
    }
}